/**
 * Created by Gu on 2016/10/24.
 */
'use strict';

function Label(label) {
    this.label = label;
    this.url = '#/technologies/' + label + '?page=1';
    this.target = '_top';
};

var MAPCOORD = {
    '黑龙江': [126.36, 45.44],
    '辽宁': [123.25, 41.48],
    '内蒙古': [111.41, 40.48],
    '山东': [117.00, 36.40],
    '山西': [112.33, 37.54],
    '河北': [114.30, 38.02],
    '河南': [113.40, 34.46],
    '陕西': [108.57, 34.17],
    '宁夏': [106.16, 38.27],
    '甘肃': [103.51, 36.04],
    '青海': [97.45, 36.20],
    '新疆': [87.36, 43.45],
    '西藏': [91.08, 29.39],
    '四川': [104.04, 30.40],
    '贵州': [106.42, 26.35],
    '云南': [102.42, 25.04],
    '广西': [108.19, 22.48],
    '海南': [110.20, 20.02],
    '澳门': [115.07, 21.33],
    '香港': [115.12, 21.23],
    '广东': [113.14, 23.08],
    '台湾': [121.30, 25.03],
    '湖北': [114.17, 30.35],
    '湖南': [112.59, 28.12],
    '浙江': [120.10, 30.16],
    '江西': [115.55, 28.40],
    '安徽': [117.17, 31.52],
    '江苏': [118.46, 32.03],
    '福建': [119.18, 26.05],
    '海门': [121.15, 31.89],
    '鄂尔多斯': [109.781327, 39.608266],
    '招远': [120.38, 37.35],
    '舟山': [122.207216, 29.985295],
    '齐齐哈尔': [123.97, 47.33],
    '盐城': [120.13, 33.38],
    '赤峰': [118.87, 42.28],
    '青岛': [120.33, 36.07],
    '乳山': [121.52, 36.89],
    '金昌': [102.188043, 38.520089],
    '泉州': [118.58, 24.93],
    '莱西': [120.53, 36.86],
    '日照': [119.46, 35.42],
    '胶南': [119.97, 35.88],
    '南通': [121.05, 32.08],
    '拉萨': [91.11, 29.97],
    '云浮': [112.02, 22.93],
    '梅州': [116.1, 24.55],
    '文登': [122.05, 37.2],
    '上海': [121.48, 31.22],
    '攀枝花': [101.718637, 26.582347],
    '威海': [122.1, 37.5],
    '承德': [117.93, 40.97],
    '厦门': [118.1, 24.46],
    '汕尾': [115.375279, 22.786211],
    '潮州': [116.63, 23.68],
    '丹东': [124.37, 40.13],
    '太仓': [121.1, 31.45],
    '曲靖': [103.79, 25.51],
    '烟台': [121.39, 37.52],
    '福州': [119.3, 26.08],
    '瓦房店': [121.979603, 39.627114],
    '即墨': [120.45, 36.38],
    '抚顺': [123.97, 41.97],
    '玉溪': [102.52, 24.35],
    '张家口': [114.87, 40.82],
    '阳泉': [113.57, 37.85],
    '莱州': [119.942327, 37.177017],
    '湖州': [120.1, 30.86],
    '汕头': [116.69, 23.39],
    '昆山': [120.95, 31.39],
    '宁波': [121.56, 29.86],
    '湛江': [110.359377, 21.270708],
    '揭阳': [116.35, 23.55],
    '荣成': [122.41, 37.16],
    '连云港': [119.16, 34.59],
    '葫芦岛': [120.836932, 40.711052],
    '常熟': [120.74, 31.64],
    '东莞': [113.75, 23.04],
    '河源': [114.68, 23.73],
    '淮安': [119.15, 33.5],
    '泰州': [119.9, 32.49],
    '南宁': [108.33, 22.84],
    '营口': [122.18, 40.65],
    '惠州': [114.4, 23.09],
    '江阴': [120.26, 31.91],
    '蓬莱': [120.75, 37.8],
    '韶关': [113.62, 24.84],
    '嘉峪关': [98.289152, 39.77313],
    '广州': [113.23, 23.16],
    '延安': [109.47, 36.6],
    '太原': [112.53, 37.87],
    '清远': [113.01, 23.7],
    '中山': [113.38, 22.52],
    '昆明': [102.73, 25.04],
    '寿光': [118.73, 36.86],
    '盘锦': [122.070714, 41.119997],
    '长治': [113.08, 36.18],
    '深圳': [114.07, 22.62],
    '珠海': [113.52, 22.3],
    '宿迁': [118.3, 33.96],
    '咸阳': [108.72, 34.36],
    '铜川': [109.11, 35.09],
    '平度': [119.97, 36.77],
    '佛山': [113.11, 23.05],
    '海口': [110.35, 20.02],
    '江门': [113.06, 22.61],
    '章丘': [117.53, 36.72],
    '肇庆': [112.44, 23.05],
    '大连': [121.62, 38.92],
    '临汾': [111.5, 36.08],
    '吴江': [120.63, 31.16],
    '石嘴山': [106.39, 39.04],
    '沈阳': [123.38, 41.8],
    '苏州': [120.62, 31.32],
    '茂名': [110.88, 21.68],
    '嘉兴': [120.76, 30.77],
    '长春': [125.35, 43.88],
    '胶州': [120.03336, 36.264622],
    '银川': [106.27, 38.47],
    '张家港': [120.555821, 31.875428],
    '三门峡': [111.19, 34.76],
    '锦州': [121.15, 41.13],
    '南昌': [115.89, 28.68],
    '柳州': [109.4, 24.33],
    '三亚': [109.511909, 18.252847],
    '自贡': [104.778442, 29.33903],
    '吉林': [126.57, 43.87],
    '阳江': [111.95, 21.85],
    '泸州': [105.39, 28.91],
    '西宁': [101.74, 36.56],
    '宜宾': [104.56, 29.77],
    '呼和浩特': [111.65, 40.82],
    '成都': [104.06, 30.67],
    '大同': [113.3, 40.12],
    '镇江': [119.44, 32.2],
    '桂林': [110.28, 25.29],
    '张家界': [110.479191, 29.117096],
    '宜兴': [119.82, 31.36],
    '北海': [109.12, 21.49],
    '西安': [108.95, 34.27],
    '金坛': [119.56, 31.74],
    '东营': [118.49, 37.46],
    '牡丹江': [129.58, 44.6],
    '遵义': [106.9, 27.7],
    '绍兴': [120.58, 30.01],
    '扬州': [119.42, 32.39],
    '常州': [119.95, 31.79],
    '潍坊': [119.1, 36.62],
    '重庆': [106.54, 29.59],
    '台州': [121.420757, 28.656386],
    '南京': [118.78, 32.04],
    '滨州': [118.03, 37.36],
    '贵阳': [106.71, 26.57],
    '无锡': [120.29, 31.59],
    '本溪': [123.73, 41.3],
    '克拉玛依': [84.77, 45.59],
    '渭南': [109.5, 34.52],
    '马鞍山': [118.48, 31.56],
    '宝鸡': [107.15, 34.38],
    '焦作': [113.21, 35.24],
    '句容': [119.16, 31.95],
    '北京': [116.46, 39.92],
    '徐州': [117.2, 34.26],
    '衡水': [115.72, 37.72],
    '包头': [110, 40.58],
    '绵阳': [104.73, 31.48],
    '乌鲁木齐': [87.68, 43.77],
    '枣庄': [117.57, 34.86],
    '杭州': [120.19, 30.26],
    '淄博': [118.05, 36.78],
    '鞍山': [122.85, 41.12],
    '溧阳': [119.48, 31.43],
    '库尔勒': [86.06, 41.68],
    '安阳': [114.35, 36.1],
    '开封': [114.35, 34.79],
    '济南': [117, 36.65],
    '德阳': [104.37, 31.13],
    '温州': [120.65, 28.01],
    '九江': [115.97, 29.71],
    '邯郸': [114.47, 36.6],
    '临安': [119.72, 30.23],
    '兰州': [103.73, 36.03],
    '沧州': [116.83, 38.33],
    '临沂': [118.35, 35.05],
    '南充': [106.110698, 30.837793],
    '天津': [117.2, 39.13],
    '富阳': [119.95, 30.07],
    '泰安': [117.13, 36.18],
    '诸暨': [120.23, 29.71],
    '郑州': [113.65, 34.76],
    '哈尔滨': [126.63, 45.75],
    '聊城': [115.97, 36.45],
    '芜湖': [118.38, 31.33],
    '唐山': [118.02, 39.63],
    '平顶山': [113.29, 33.75],
    '邢台': [114.48, 37.05],
    '德州': [116.29, 37.45],
    '济宁': [116.59, 35.38],
    '荆州': [112.239741, 30.335165],
    '宜昌': [111.3, 30.7],
    '义乌': [120.06, 29.32],
    '丽水': [119.92, 28.45],
    '洛阳': [112.44, 34.7],
    '秦皇岛': [119.57, 39.95],
    '株洲': [113.16, 27.83],
    '石家庄': [114.48, 38.03],
    '莱芜': [117.67, 36.19],
    '常德': [111.69, 29.05],
    '保定': [115.48, 38.85],
    '湘潭': [112.91, 27.87],
    '金华': [119.64, 29.12],
    '岳阳': [113.09, 29.37],
    '长沙': [113, 28.21],
    '衢州': [118.88, 28.97],
    '廊坊': [116.7, 39.53],
    '菏泽': [115.480656, 35.23375],
    '合肥': [117.27, 31.86],
    '武汉': [114.31, 30.52],
    '大庆': [125.03, 46.58]
};

var MetisPlugin = {
        colorSet: ['#cf532f', '#f86f00', '#ffc65d', '#619504', '#2162b6', '#7a3794', '#d1294c'],
        DrawHeatMap: function (element, dataSets, mapJson, type) {

            /**
             * 给数据添加坐标
             * @param data
             * @returns {Array}
             */
            var convertData = function (data, isPow) {
                var res = [];
                for (var i in data) {
                    var geoCoord = MAPCOORD[i];
                    if (geoCoord) {
                        res.push({
                            name: i,
                            value: geoCoord.concat(isPow ? Math.pow(data[i], .5) : data[i])
                        });
                    }
                }
                return res;
            };

            var color = {
                import: ['#f7b52d', '#f86f00', '#cf532f'],
                export: ['#00a0e9', '#1f86ed', '#2162b6']
            }, legendName = ['技术输出', '技术吸纳'], defaultOption = {
                color: ['#1f86ed', '#f79a01'],
                tooltip: {
                    trigger: 'item',
                    formatter: function (params) {
                        return params.data.name ? (params.data.name + params.seriesName) : (params.data[0] + params.seriesName);
                    }
                },
                legend: {
                    y: 'bottom',
                    x: 'right',
                    itemGap: 15,
                    data: legendName,
                    textStyle: {
                        color: '#fff'
                    },
                    selectedMode: 'single',
                    icon: 'pin',
                    selected: {}
                },
                visualMap: {
                    itemHeight: 60,
                    calculable: true,
                    min: 0,
                    max: type === '总量' ? 50 : 1,
                    splitNumber: 5,
                    inRange: {
                        color: ['#00a0e9', '#1f86ed', '#2162b6']
                    },
                    textStyle: {
                        color: '#ddd'
                    },
                    formatter: function (params) {
                        // console.log(params);
                        return type === '总量' ? Math.round(params * 10) / 10 : Math.round(params * 1000) / 10 + '%';
                    }
                }
                ,
                series: [{
                    name: '',
                    type: 'map',
                    map: 'china',
                    showLegendSymbol: false,
                    data: []
                }, {
                    name: '',
                    type: 'map',
                    map: 'china',
                    itemStyle: {
                        emphasis: {
                            areaColor: '#44bae5'
                        }
                    },
                    showLegendSymbol: false,
                    data: []
                }]
            };

            defaultOption.legend.selected[legendName[0]] = true;
            defaultOption.series[0].name = legendName[0];
            defaultOption.series[1].name = legendName[1];
            defaultOption.series[0].data = convertData(dataSets[0], type === '总量');
            defaultOption.series[1].data = convertData(dataSets[1], type === '总量');

            $(element).height($(element).width() * .9);// 基于准备好的dom，初始化echarts实例

            echarts.registerMap('china', mapJson);
            var scatterMap = echarts.init(element);

            // 使用刚指定的配置项和数据显示图表。
            scatterMap.setOption(defaultOption);
            scatterMap.on('legendselectchanged', function (param) {
                if (param.name === ('技术吸纳')) {
                    scatterMap.setOption({
                        visualMap: {
                            inRange: {
                                color: color.import
                            }
                        }
                    });
                } else {
                    scatterMap.setOption({
                        visualMap: {
                            inRange: {
                                color: color.export
                            }
                        }
                    });
                }
            });

            return scatterMap;
            // }
        },
        DrawScatterCoord: function (element, dataSet, option) {
            $(element).height($(element).width() / 1.5);// 基于准备好的dom，初始化echarts实例

            var data = [
                [[28604, 77, 17096869, 'Australia', 1990], [31163, 77.4, 27662440, 'Canada', 1990], [1516, 68, 1154605773, 'China', 1990], [13670, 74.7, 10582082, 'Cuba', 1990], [28599, 75, 4986705, 'Finland', 1990], [29476, 77.1, 56943299, 'France', 1990], [31476, 75.4, 78958237, 'Germany', 1990], [28666, 78.1, 254830, 'Iceland', 1990], [1777, 57.7, 870601776, 'India', 1990], [29550, 79.1, 122249285, 'Japan', 1990], [2076, 67.9, 20194354, 'North Korea', 1990], [12087, 72, 42972254, 'South Korea', 1990], [24021, 75.4, 3397534, 'New Zealand', 1990], [43296, 76.8, 4240375, 'Norway', 1990], [10088, 70.8, 38195258, 'Poland', 1990], [19349, 69.6, 147568552, 'Russia', 1990], [10670, 67.3, 53994605, 'Turkey', 1990], [26424, 75.7, 57110117, 'United Kingdom', 1990], [37062, 75.4, 252847810, 'United States', 1990]],
                [[44056, 81.8, 23968973, 'Australia', 2015], [43294, 81.7, 35939927, 'Canada', 2015], [13334, 76.9, 1376048943, 'China', 2015], [21291, 78.5, 11389562, 'Cuba', 2015], [38923, 80.8, 5503457, 'Finland', 2015], [37599, 81.9, 64395345, 'France', 2015], [44053, 81.1, 80688545, 'Germany', 2015], [42182, 82.8, 329425, 'Iceland', 2015], [5903, 66.8, 1311050527, 'India', 2015], [36162, 83.5, 126573481, 'Japan', 2015], [1390, 71.4, 25155317, 'North Korea', 2015], [34644, 80.7, 50293439, 'South Korea', 2015], [34186, 80.6, 4528526, 'New Zealand', 2015], [64304, 81.6, 5210967, 'Norway', 2015], [24787, 77.3, 38611794, 'Poland', 2015], [23038, 73.13, 143456918, 'Russia', 2015], [19360, 76.5, 78665830, 'Turkey', 2015], [38225, 81.4, 64715810, 'United Kingdom', 2015], [53354, 79.1, 321773631, 'United States', 2015]]
            ];

            var scatterCoord = echarts.init(element);

            var default_option = {
                xAxis: {
                    splitLine: {
                        lineStyle: {
                            type: 'dashed'
                        }
                    }
                },
                yAxis: {
                    splitLine: {
                        lineStyle: {
                            type: 'dashed'
                        }
                    },
                    scale: true
                },
                series: [{
                    name: '1990',
                    data: data[0],
                    type: 'scatter',
                    symbolSize: function (data) {
                        return Math.sqrt(data[2]) / 5e2;
                    },
                    label: {
                        emphasis: {
                            show: true,
                            formatter: function (param) {
                                return param.data[3];
                            },
                            position: 'top'
                        }
                    },
                    itemStyle: {
                        normal: {
                            shadowBlur: 10,
                            shadowColor: 'rgba(120, 36, 50, 0.5)',
                            shadowOffsetY: 5,
                            color: new echarts.graphic.RadialGradient(0.4, 0.3, 1, [{
                                offset: 0,
                                color: 'rgb(251, 118, 123)'
                            }, {
                                offset: 1,
                                color: 'rgb(204, 46, 72)'
                            }])
                        }
                    }
                }, {
                    name: '2015',
                    data: data[1],
                    type: 'scatter',
                    symbolSize: function (data) {
                        return Math.sqrt(data[2]) / 5e2;
                    },
                    label: {
                        emphasis: {
                            show: true,
                            formatter: function (param) {
                                return param.data[3];
                            },
                            position: 'top'
                        }
                    },
                    itemStyle: {
                        normal: {
                            shadowBlur: 10,
                            shadowColor: 'rgba(25, 100, 150, 0.5)',
                            shadowOffsetY: 5,
                            color: new echarts.graphic.RadialGradient(0.4, 0.3, 1, [{
                                offset: 0,
                                color: 'rgb(129, 227, 238)'
                            }, {
                                offset: 1,
                                color: 'rgb(25, 183, 207)'
                            }])
                        }
                    }
                }]
            };

            scatterCoord.setOption(option || default_option);
        },
        DrawRadar: function (element, dataSet) {
            $(element).height($(element).width());

            // 基于准备好的dom，初始化echarts实例
            var radar = echarts.init(element);

            var radarOption = {
                    radar: [{
                        indicator: [
                            {name: '擅长领域', max: 100},
                            {name: '专利数量', max: 100},
                            {name: '自主研发', max: 100},
                            {name: '实用能力', max: 100},
                            {name: '科研能力', max: 100}
                        ],
                        center: ['50%', '50%'],
                        radius: 80
                    }],
                    series: [{
                        type: 'radar',
                        data: [
                            {
                                value: [85, 90, 90, 95, 95],
                                name: '某主食手机'
                            },
                            {
                                value: [95, 80, 95, 90, 93],
                                name: '某水果手机'
                            }
                        ]
                    }]
                }
                ;

            // 使用刚指定的配置项和数据显示图表。
            radar.setOption(radarOption);
        },
        DrawRosePie: function (element, dataSet) {
            $(element).height($(element).width());
            var rosePieChart = echarts.init(element);
            rosePieChart.setOption({
                calculable: true,
                series: [
                    {
                        name: '面积模式',
                        type: 'pie',
                        radius: [30, 110],
                        center: ['50%', '50%'],
                        roseType: 'area',
                        data: [
                            {value: 10, name: 'rose1'},
                            {value: 5, name: 'rose2'},
                            {value: 15, name: 'rose3'},
                            {value: 25, name: 'rose4'},
                            {value: 20, name: 'rose5'},
                            {value: 35, name: 'rose6'},
                            {value: 30, name: 'rose7'},
                            {value: 40, name: 'rose8'}
                        ]
                    }
                ]
            });
        },
        DrawCompareBar: function (element, dataSet, option) {
            var defaultOption = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                        type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                    },
                    formatter: function (params, ticket, callback) {
                        return params[0].name + '<br/>' + params[0].seriesName + ':' + params[0].value / (params[0].value + params[1].value) * 100 + '%<br/>' + params[1].seriesName + ':' + params[1].value / (params[0].value + params[1].value) * 100 + '%'
                    }
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: {
                    type: 'value',
                    interval: 10,
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        interval: function (a, b) {
                            console.log(a, b);
                        }
                    }
                },
                yAxis: {
                    type: 'category',
                    data: ['分散度', '透明度', '流动性', '出入比', '成交额', '发展指数', '综合评分'],
                    axisTick: {
                        show: false
                    }
                },
                series: [
                    {
                        name: '直接访问',
                        type: 'bar',
                        barWidth: 15,
                        stack: '总量',
                        label: {
                            normal: {
                                show: false
                                // position: 'insideRight'
                            }
                        },
                        data: [30, 32, 40, 20, 62, 12, 82]
                    },
                    {
                        name: '邮件营销',
                        type: 'bar',
                        stack: '总量',
                        label: {
                            normal: {
                                show: false
                                // position: 'insideRight'
                            }
                        },
                        data: [70, 68, 60, 80, 38, 88, 18]
                    }
                ]
            };

            var compareBarChart = echarts.init(element);

            compareBarChart.setOption(option || defaultOption);

            return compareBarChart;
        },
        DrawParallel: function (element, dataSet) {
            var schema = [
                {name: 'date', index: 0, text: '日期'},
                {name: 'AQIindex', index: 1, text: 'AQI'},
                {name: 'PM25', index: 2, text: 'PM2.5'},
                {name: 'PM10', index: 3, text: 'PM10'},
                {name: 'CO', index: 4, text: ' CO'},
                {name: 'NO2', index: 5, text: 'NO2'},
                {name: 'SO2', index: 6, text: 'SO2'},
                {name: '等级', index: 7, text: '等级'}
            ];

            $(element).height($(element).width() / 2);
            var compareBarChart = echarts.init(element);
            compareBarChart.setOption({
                color: ['#2b8ff2', '#00ffbe', '#ffc760', '#4fccff', '#ff5722', '#fb497c'],
                legend: {
                    bottom: 30,
                    data: ['上海', '北京', '广州'],
                    orient: 'vertical',
                    left: 20,
                    top: 60,
                    align: 'right',
                    itemWidth: 0,
                    textStyle: {
                        color: '#fff',
                        fontSize: 14
                    }
                },
                tooltip: {
                    padding: 10,
                    backgroundColor: '#222',
                    borderColor: '#777',
                    borderWidth: 1,
                    formatter: function (obj) {
                        var value = obj[0].value;
                        return '<div style="border-bottom: 1px solid rgba(255,255,255,.3); font-size: 18px;padding-bottom: 7px;margin-bottom: 7px">'
                            + obj[0].seriesName + ' ' + value[0] + '日期：'
                            + value[7]
                            + '</div>'
                            + schema[1].text + '：' + value[1] + '<br>'
                            + schema[2].text + '：' + value[2] + '<br>'
                            + schema[3].text + '：' + value[3] + '<br>'
                            + schema[4].text + '：' + value[4] + '<br>'
                            + schema[5].text + '：' + value[5] + '<br>'
                            + schema[6].text + '：' + value[6] + '<br>';
                    }
                },
                parallelAxis: [
                    {dim: 0, name: schema[0].text, inverse: true, max: 31, nameLocation: 'start'},
                    {dim: 1, name: schema[1].text},
                    {dim: 2, name: schema[2].text},
                    {dim: 3, name: schema[3].text},
                    {dim: 4, name: schema[4].text},
                    {dim: 5, name: schema[5].text},
                    {dim: 6, name: schema[6].text},
                    {
                        dim: 7, name: schema[7].text,
                        type: 'category', data: ['优', '良', '轻度污染', '中度污染', '重度污染', '严重污染']
                    }
                ],
                parallel: {
                    parallelAxisDefault: {
                        type: 'value',
                        name: 'AQI指数',
                        nameLocation: 'end',
                        nameGap: 20,
                        nameTextStyle: {
                            color: '#fff',
                            fontSize: 12
                        },
                        axisLine: {
                            lineStyle: {
                                color: '#aaa'
                            }
                        },
                        axisTick: {
                            lineStyle: {
                                color: '#777'
                            }
                        },
                        splitLine: {
                            show: false
                        },
                        axisLabel: {
                            textStyle: {
                                color: '#fff'
                            }
                        }
                    }
                },
                series: [
                    {
                        name: '上海',
                        type: 'parallel',
                        lineStyle: {
                            normal: {
                                width: 1,
                                opacity: 0.5
                            }
                        },
                        data: [
                            [1, 91, 45, 125, 0.82, 34, 23, "良"]
                        ]
                    }, {
                        name: '北京',
                        type: 'parallel',
                        lineStyle: {
                            normal: {
                                width: 1,
                                opacity: 0.5
                            }
                        },
                        data: [
                            [11, 117, 81, 124, 1.03, 45, 24, "轻度污染"]
                        ]
                    }, {
                        name: '广州',
                        type: 'parallel',
                        lineStyle: {
                            normal: {
                                width: 1,
                                opacity: 0.5
                            }
                        },
                        data: [
                            [27, 39, 24, 39, 0.59, 50, 19, "优"]
                        ]
                    }
                ]
            });
        },
        DrawProvince: function (element, dataSet, option) {
            var mapChart = echarts.init(element);
            element.childNodes[0].classList.add('animated');
            element.childNodes[0].classList.add('fadeIn');
            echarts.registerMap(option.series[0].mapType, dataSet);
            mapChart.setOption(option);
            return mapChart;
        },
        DrawFunction: function (element, dataSet, funcs, option) {

            var defaultOption = {
                color: ['#2b8ff2', '#00ffbe', '#ffc760', '#4fccff', '#ff5722', '#fb497c'],
                smooth: true,
                title: {
                    text: '相关研究近年来的研究进展曲线',
                    textStyle: {fontWeight: 'normal'},
                    left: 20
                },
                grid: {
                    left: '-2%',
                    right: '4%',
                    bottom: '10%',
                    containLabel: true
                },
                xAxis: {
                    type: 'value',
                    splitLine: {
                        show: false,
                    },
                    axisTick: {
                        show: false
                    },
                    max: 'dataMax',
                    min: 'dataMin',
                    splitNumber: 13,
                    axisLabel: {
                        show: false
                    }
                },
                yAxis: {
                    type: 'value',
                    axisLine: {
                        show: false
                    },
                    min: 0,
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    }
                },
                series: []
            };

            var N_POINT = $(element).width();
            var series = [];
            var myBarChart;

            if (dataSet && option) {

                var data = [];

                for (var i in dataSet) {
                    data.push([i, dataSet[i].trade_sum, dataSet[i].trade_num]);
                }

                option.series[0].data = data;

                // console.log(option);

                myBarChart = echarts.init(element);
                myBarChart.setOption(option);
            } else {

                if (funcs && funcs.length > 0) {
                    for (var j = 0; j < funcs.length; j++) {
                        var data = [];
                        for (var i = 1; i <= N_POINT; i++) {
                            var x = 3 * i / N_POINT;
                            if (x >= 0.5) {
                                var y = funcs[j](x);
                                data.push([x, y]);
                            }
                        }
                        series.push({
                            name: name + 'j',
                            type: 'line',
                            lineStyle: {
                                normal: {
                                    width: 1,
                                    color: '#8ed7e2'
                                }
                            },
                            data: data,
                            showSymbol: false
                        });
                    }
                }

                myBarChart = echarts.init(element);

                // 使用刚指定的配置项和数据显示图表。
                if (option) {
                    option.series = option.series.concat(series);
                    myBarChart.setOption(option);
                } else {
                    defaultOption.series = series;
                    myBarChart.setOption(defaultOption);
                }
            }
            return myBarChart;
        },
        DrawDependencyGraph: function (element, dataSet, option) {
            var graph = {
                d3: {
                    links: [],
                    nodes: []
                }
            };

            //=====data
            var mg = dataSet.data, node = [], link = [], category = dataSet.category;
            graph.d3.nodes[0] = {};
            graph.d3.nodes[0].id = '0';
            graph.d3.nodes[0].name = mg.n0;
            graph.d3.nodes[0].group = category[0];

            for (var j = 1; j < Object.keys(mg).length; j++) {
                for (var i = 0; i < mg[Object.keys(mg)[j]].length; i++) {
                    var temp_node = {};
                    temp_node.id = j + '' + i;
                    temp_node.name = mg[Object.keys(mg)[j]][i];
                    temp_node.group = category[j];
                    graph.d3.nodes.push(temp_node);
                    graph.d3.links.push({
                        value: 1,
                        source: graph.d3.nodes[graph.d3.nodes.length - 1].id,
                        target: graph.d3.nodes[0].id
                    });
                }
            }

            graph.d3.Init = function () {
                var svg = d3.select(element),
                    width = element.clientWidth,
                    height = +svg.attr("height");

                var color = d3.scaleOrdinal(d3.schemeCategory20);

                var simulation = d3.forceSimulation()
                    .force("link", d3.forceLink().id(function (d, i) {
                        return d.id || i; //默认返回id做匹配，如无则返回序号
                    }).distance(100))
                    .force("charge", d3.forceManyBody().strength(-320))
                    .force("center", d3.forceCenter(width / 2, height / 2));

                var link = svg.append("g")
                    .attr("class", "links")
                    .selectAll("line")
                    .data(graph.d3.links)
                    .enter().append("line")
                    .attr("stroke", "#fff")
                    .attr("stroke-width", function (d) {
                        return Math.sqrt(d.value);
                    });

                var node_container = svg.append("g").attr("class", "nodes");
                var node = node_container
                        .selectAll("rect")
                        .data(graph.d3.nodes)
                        .enter().append("rect")
                        .attr("height", 30)
                        .attr("width", function (d) {
                            $(this).after(d3.creator("text"));
                            return 5 + (d.name.length > 6 ? 6 : d.name.length) * 18;//处理方块宽度
                        })
                        .attr("name", function (d, i) {
                            return i;
                        })
                        .attr("fill", function (d) {
                            return color(d.group);
                        })
                    ;


                var text = node_container.selectAll("text")
                        .data(graph.d3.nodes)
                        .attr("dx", 0)
                        .attr("dy", 24)
                        .attr("font-size", 14)
                        .attr("fill", "#fff")
                        .attr("name", function (d, i) {
                            return i;
                        })
                        .text(function (d) {
                            return d.name.slice(0, 6);
                        }).call(d3.drag()
                            .on("start", dragstart)
                            .on("drag", drag)
                            .on("end", dragend))
                    ;

                node.append("title")
                    .text(function (d) {
                        return d.name;
                    });
                text.append("title")
                    .text(function (d) {
                        return d.name;
                    });

                simulation
                    .nodes(graph.d3.nodes)
                    .on("tick", ticked);

                simulation.force("link")
                    .links(graph.d3.links);

                /**
                 * 定位点和线的位置
                 */
                function ticked() {
                    link
                        .attr("x1", function (d) {
                            return d.source.x;
                        })
                        .attr("y1", function (d) {
                            return d.source.y;
                        })
                        .attr("x2", function (d) {
                            return d.target.x;
                        })
                        .attr("y2", function (d) {
                            return d.target.y;
                        });

                    node
                        .attr("rx", 2)
                        .attr("ry", 2)
                        .attr("x", function (d) {
                            return d.x - this.width.baseVal.value / 2;
                        })
                        .attr("y", function (d) {
                            return d.y - this.height.baseVal.value / 2;
                        });
                    text
                        .attr("x", function (d) {
                            return d.x - this.clientWidth / 2;
                        })
                        .attr("y", function (d) {
                            return d.y - this.clientHeight - 4;
                        })
                }

                $(element).on('mouseover', 'rect', function (event) {
                    $(this).attr('class', 'active');
                });
                $(element).on('mouseout', 'rect', function (event) {
                    $(this).attr('class', '');
                });

                $(element).on('mouseover', 'text', function (event) {
                    $(element).find('rect[name="' + $(this).attr('name') + '"]').attr('class', 'node active');
                });
                $(element).on('mouseout', 'text', function (event) {
                    $(element).find('rect[name="' + $(this).attr('name') + '"]').attr('class', 'node');
                });

                function dragstart(d) {
                    if (!d3.event.active) simulation.alphaTarget(0.3).restart();
                    d.fx = d.x;
                    d.fy = d.y;
                }

                function drag(d) {
                    d.fx = d3.event.x;
                    d.fy = d3.event.y;
                }

                function dragend(d) {
                    if (!d3.event.active) simulation.alphaTarget(0.5);
                    d.fx = null;
                    d.fy = null;
                }
            };

            graph.d3.Init();
        }
        ,
        DrawTagCloud: function (element, dataSet) {

            // console.log($(element).width(), dataSet);

            function Label(label) {
                this.label = label;
                this.url = '#/technologies/' + label + '?page=1';
                this.target = '_top';
            };

            var formatted_entries = [];

            for (var index = 0; index < dataSet.length; index++) {
                var temp_label = new Label(dataSet[index]);
                formatted_entries.push(temp_label);
            }

            var settings = {
                entries: formatted_entries,
                width: '100%',
                height: '100%',
                radius: '65%',
                radiusMin: 0,
                bgColor: 'transparent',
                opacityOver: 1.00,
                opacityOut: 0.05,
                opacitySpeed: 6,
                fov: 800,
                speed: .4,
                fontFamily: 'inherit',
                fontSize: '15',
                fontColor: '#fff',
                fontWeight: 'normal',//bold
                fontStyle: 'normal',//italic
                fontStretch: 'normal',//wider, narrower, ultra-condensed, extra-condensed, condensed, semi-condensed, semi-expanded, expanded, extra-expanded, ultra-expanded
                fontToUpperCase: true
            };

            $(element).height($(element).width());
            $(element).svg3DTagCloud(settings);
        }
        ,
        DrawLineBar: function (element, dataSet, option) {
            // console.log(element);
            $(element).height($(element).width() * .814);
            var lineBarChart = echarts.init(element);

            var default_option = {
                tooltip: {
                    trigger: 'axis'
                },
                toolbox: {
                    feature: {
                        dataView: {show: true, readOnly: false},
                        magicType: {show: true, type: ['line', 'bar']},
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },
                legend: {
                    data: ['输出量', '引入量']
                },
                xAxis: [
                    {
                        type: 'category',
                        data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月']
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '水量',
                        min: 0,
                        max: 250,
                        interval: 50,
                        axisLabel: {
                            formatter: '{value} ml'
                        }
                    },
                    {
                        type: 'value',
                        name: '温度',
                        min: 0,
                        max: 25,
                        interval: 5,
                        axisLabel: {
                            formatter: '{value} °C'
                        }
                    }
                ],
                series: [
                    {
                        name: '蒸发量',
                        type: 'bar',
                        data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]
                    },
                    {
                        name: '降水量',
                        type: 'bar',
                        data: [2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3]
                    },
                    {
                        name: '平均温度',
                        type: 'line',
                        yAxisIndex: 1,
                        data: [2.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2]
                    }
                ]
            };

            lineBarChart.setOption(option || default_option);

            return lineBarChart;
        }
    }
    ;
