'use strict';

angular.module('MBD').factory('mWechat', ['$rootScope', '$location', '$window', 'mResources', function ($rootScope, $location, $window, mResources) {
    var func = {};

    var CONFIG = {
        weChatJsDebug: false,
        weChatShareAppId: 'wxbeafb762dfd1917a'
    };

    var createNonceStr = function () {
        return Math.random().toString(36).substr(2, 15);
    };

    var createTimestamp = function () {
        return parseInt(new Date().getTime() / 1000) + '';
    };


    func.params = function () {
        var url = window.location.href;
        return {
            noncestr: createNonceStr(),
            timestamp: createTimestamp(),
            url: url
        };
    };

    func.config = function (params) {
        wx.config({
            debug: CONFIG.weChatJsDebug, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
            appId: CONFIG.weChatShareAppId, // 必填，公众号的唯一标识
            timestamp: params.timestamp, // 必填，生成签名的时间戳
            nonceStr: params.noncestr, // 必填，生成签名的随机串
            signature: params.signature,// 必填，签名，见附录1
            jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareWeibo', 'onMenuShareQZone'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
        });

        wx.error(function (res) {

        });
    };

    func.share = function (data) {

        var params = func.params();

        mResources.config.wechat(null, params, function (result) {
            params.signature = result.signature;
            func.config(params);
            func.shareWechat(data);
        });

    };

    func.shareWechat = function (data) {
        var data = data || {};
        data.title = data.title || angular.element('title').html();
        data.desc = data.desc || angular.element('meta[name="description"]').attr('content');
        data.link = data.link || $location.absUrl();
        data.imgUrl = data.imgUrl || 'http://www.maikeji.cn/favicon.ico';
        wx.ready(function () {
            wx.onMenuShareAppMessage({
                title: data.title, // 分享标题
                desc: data.desc, // 分享描述
                link: data.link, // 分享链接
                imgUrl: data.imgUrl, // 分享图标
                type: 'link', // 分享类型,music、video或link，不填默认为link
                //dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
                success: function () {
                    // 用户确认分享后执行的回调函数
                    data.success();
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                    data.cancel();
                }
            });

            wx.onMenuShareTimeline({
                title: data.title, // 分享标题
                link: data.link, // 分享链接
                imgUrl: data.imgUrl, // 分享图标
                success: function () {
                    // 用户确认分享后执行的回调函数
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });

            wx.onMenuShareQQ({
                title: data.title, // 分享标题
                desc: data.desc, // 分享描述
                link: data.link, // 分享链接
                imgUrl: data.imgUrl, // 分享图标
                success: function () {
                    // 用户确认分享后执行的回调函数
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });

            wx.onMenuShareWeibo({
                title: data.title, // 分享标题
                desc: data.desc, // 分享描述
                link: data.link,// 分享链接
                imgUrl: data.imgUrl, // 分享图标
                success: function () {
                    // 用户确认分享后执行的回调函数
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });

            wx.onMenuShareQZone({
                title: data.title, // 分享标题
                desc: data.desc, // 分享描述
                link: data.link, // 分享链接
                imgUrl: data.imgUrl, // 分享图标
                success: function () {
                    // 用户确认分享后执行的回调函数
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });

        })
    };

    return func;
}
])
;
