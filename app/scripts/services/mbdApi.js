'use strict';

angular.module('MBD').factory('API', [
    '$resource', function($resource) {
        //var apiHost = 'http://114.55.73.197:8000',
        //    apiTestHost = 'http://114.55.73.197:8000';
        var apiHost = 'https://api.maikeji.cn:7000',
            apiTestHost = 'https://api.maikeji.cn:7000';


        return $resource(apiTestHost, null, {
            'segment': {//分词接口
                url: apiTestHost + '/segment',
                method: 'POST',
                isArray: true,
            },
            'searchBasic': {//搜索结果
                url: apiTestHost + '/search_basic',
                method: 'POST',
                isArray: false,
            },
            'searchRank': {//人才机构排名
                url: apiTestHost + '/search_rank',
                method: 'POST',
                isArray: false,
            },
            'searchDistribution': {//地域分布
                url: apiTestHost + '/search_distribution',
                method: 'POST',
                isArray: false,
            },
            'searchPrediction': {//预测函数参数
                url: apiTestHost + '/search_prediction',
                method: 'POST',
                isArray: false,
            },
            'searchTagCloud': {//搜索返回的关键词云
                url: apiTestHost + '/search_tag_cloud',
                method: 'POST',
                isArray: false,
            },
            'patentInfo': {//技术详情
                url: apiTestHost + '/patent_info',
                method: 'POST',
                isArray: false,
            },
            'patentTagCloud': {//技术关键词云
                url: apiHost + '/patent_tag_cloud',
                method: 'POST',
                isArray: false,
            },
            'patentPrediction': {//技术预测
                url: apiTestHost + '/patent_prediction',
                method: 'POST',
                isArray: false,
            },
            'patentRelated': {//技术相关人才。机构
                url: apiTestHost + '/patent_related',
                method: 'POST',
                isArray: false,
            },
            'expert': {//专家
                url: apiTestHost + '/talent_detail',

                method: 'POST',
                isArray: false,
            },
            'technology': {//热门技术
                url: 'https://api.metalab.cn/technologies?_range=1,1&_sort=-pageViews',
                method: 'GET',
                isArray: false,
            },
            'location': {//当前ip地址
                url: 'http://restapi.amap.com/v3/ip?key=4e327ac1493ed42049f496fef571a7c9',
                method: 'GET',
                isArray: false,
            },
            'industryInfo': {//当前ip地址
                url: apiTestHost + '/fi/basic',
                method: 'POST',
                isArray: false,
            },
            'institutionInfo': {//当前ip地址
                url: apiTestHost + '/fi/institution',
                method: 'POST',
                isArray: false,
            },
            'localKeyIndustry': {//选中地区的特色产业及其关键字
                url: apiTestHost + '/local_key_industry',
                method: 'GET',
                isArray: true,
            },
            'nationalDistribution': {//产业的全国分布
                url: apiTestHost + '/national_distribution',
                method: 'GET',
                isArray: false,
            },
            'relatedIns': {//某领域在某地区的相关公司、学校、机构
                url: apiTestHost + '/related_ins',
                method: 'GET',
                isArray: false,
            },
            'companyInfo': {//公司详情
                url: apiTestHost + '/ins_detail',
                method: 'GET',
                isArray: false,
            },
            'industryEvaluation': {//行业评价指数
                url: apiTestHost + '/evaluation',
                method: 'GET',
                isArray: false,
            },
            'industryDevelopment': {//技术发展参数-年
                url: apiTestHost + '/industry_development',
                method: 'GET',
                isArray: false,
            },
            'industryRecommended': {//推荐行业
                url: apiTestHost + '/recommended',
                method: 'GET',
                isArray: false,
            },
            'techCurve': {//技术发展散列点
                url: apiTestHost + '/tech_curve',
                method: 'GET',
                isArray: false,
            },
            'techTradeMicro': {//宏观市场参数
                url: apiTestHost + '/macro_tech_trade_parameters',
                method: 'GET',
                isArray: false,
            },
            'marketHeat': {//国家各个城市的热度参数
                url: apiTestHost + '/heat_distribution',
                method: 'GET',
                isArray: false,
            },
            'techTradeRegion': {//技术参数
                url: apiTestHost + '/region_tech_trade_parameters',
                method: 'GET',
                isArray: false,
            },
            'techMarketInfo': {//市场统计信息集合
                url: apiTestHost + '/market',
                method: 'GET',
                isArray: false,
            },
            'relatedKeywords': {//地区特色产业相关关键词
                url: apiTestHost + '/related_kw',
                method: 'GET',
                isArray: false,
            },
        });
    }]);
