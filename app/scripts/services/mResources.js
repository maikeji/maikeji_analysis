/**
 * Created by dengyu on 2017/1/5.
 */
'use strict';

angular.module('MBD').factory('mResources', ['$resource', function ($resource) {
    var url = function (path) {
        return 'https://api.metalab.cn' + path;
    };
    var func = {};

    func.config = $resource(url('/config'), null, {
        wechat: {
            url: url('/config/wechat_signature'),
            method: 'POST',
            isArray: false
        }
    });

    return func;
}]);
