/**
 * Created by Gu on 2016/12/2.
 */
'use strict';

/**
 * hide the element when you click [everywhere]
 */
angular.module('MBD').directive('globalClickClose', [function () {
  return {
    restrict: 'A',
    link: function (scope, elem, attr) {
      $(window).on('click', function (e) {
        if (scope.vm.market.dpd[attr.globalClickClose].show && elem.children().index(e.target) === -1 && elem.index(e.target) === -1) {
          scope.vm.market.dpd[attr.globalClickClose].show = false;
          scope.$apply();
        }
      });
    }
  };
}]);
