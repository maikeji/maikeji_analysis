/**
 * Created by Gu on 2016/11/23.
 */
'use strict';

/**
 * Metis technology tags Directive
 */
angular.module('MBD').directive('techTradeYears', ['CONSTDATA', '$timeout', function (CONSTDATA, $timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.InitTimeCombi = function (datas, option) {
                element.height(600);
                var timeCombi = echarts.init(element[0]);
                $timeout(function () {
                        //分布二维坐标图
                        timeCombi.setOption({
                            baseOption: {
                                color: ['#2b8ff2', '#00ffbe', '#ffc760', '#4fccff', '#ff5722', '#fb497c'],
                                tooltip: {
                                    show: true,
                                    extraCssText: 'text-align:left',
                                    formatter: function (params) {
                                        switch (params.seriesType) {
                                            case 'scatter':
                                                if (params.data.value[4]) {
                                                    return params.name + '<br>交易额：' + Math.round(params.data.value[4] * 100) / 100 + '<br>交易量：' + Math.round(params.data.value[5] * 100) / 100 + '<br>转化交易率：' + Math.round(params.data.value[6] * 10000) / 100 + '%';
                                                } else {
                                                    return params.name + '<br>交易额：' + Math.round(params.data.value[0] * 100) / 100 + '<br>交易量：' + Math.round(params.data.value[1] * 100) / 100 + '<br>转化交易率：' + Math.round(params.data.value[3] * 10000) / 100 + '%';
                                                }
                                                break;
                                            case 'pie':
                                                break;
                                            case 'radar':
                                                return params;
                                                break;
                                            default:
                                                return null;
                                                break;
                                        }
                                    }
                                },
                                timeline: {
                                    autoPlay: true,
                                    axisType: 'category',
                                    left: 0,
                                    top: 0,
                                    bottom: 0,
                                    width: 44,
                                    height: '90%',
                                    orient: 'vertical',
                                    symbol: 'circle',
                                    symbolSize: 8,
                                    checkpointStyle: {
                                        symbolSize: 9,
                                        color: '#1f86ed',
                                        borderWidth: 1,
                                        borderColor: '#fff'
                                    },
                                    controlStyle: {
                                        showPrevBtn: false,
                                        showNextBtn: false,
                                        itemSize: 20,
                                        position: 'top',
                                        normal: {
                                            color: '#efefef',
                                            borderColor: '#efefef',
                                        }
                                    },
                                    lineStyle: {
                                        color: '#efefef'
                                    },
                                    itemStyle: {
                                        normal: {
                                            color: '#efefef'
                                        }
                                    },
                                    label: {
                                        position: 'left',
                                        normal: {
                                            textStyle: {
                                                color: '#efefef'
                                            }
                                        }
                                    },
                                    data: ['2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015']
                                },
                                grid: {
                                    height: '50%',
                                    left: '20%',
                                    right: '3.6%',
                                    top: 30
                                },
                                xAxis: {
                                    name: '领域交易量',
                                    nameLocation: 'middle',
                                    nameTextStyle: {
                                        color: '#888888'
                                    },
                                    axisLabel: {
                                        show: false,
                                        textStyle: {
                                            color: "#888888"
                                        }
                                    },
                                    axisTick: {
                                        textStyle: {
                                            color: "#888888"
                                        }
                                    },
                                    axisLine: {
                                        show: false
                                    },
                                    splitLine: {
                                        lineStyle: {
                                            type: 'dashed',
                                            color: '#888888'
                                        }
                                    }
                                },
                                yAxis: {
                                    name: '领域交易额',
                                    nameRotate: 90,
                                    nameLocation: 'middle',
                                    nameTextStyle: {
                                        color: '#888888'
                                    },
                                    axisLabel: {
                                        show: false,
                                        textStyle: {
                                            color: "#888888"
                                        }
                                    },
                                    axisTick: {
                                        textStyle: {
                                            color: "#888888"
                                        }
                                    },
                                    axisLine: {
                                        show: false
                                    },
                                    splitLine: {
                                        lineStyle: {
                                            type: 'dashed',
                                            color: '#888888'
                                        }
                                    },
                                    scale: true
                                },
                                radar: [{
                                    indicator: [
                                        {name: '成长\n指数', max: 1},
                                        {name: ' 市场\n 价值\n(亿元)', max: 10000},
                                        {name: '自发  \n交易率\n(%)', max: 100},
                                        {name: '市场\n规模\n(亿元)', max: 110000},
                                        {name: '  技术\n含金量\n(亿元)', max: 1000}
                                    ],
                                    center: ['40%', '78%'],
                                    radius: '20%',
                                    splitNumber: 1,
                                    splitArea: {
                                        show: false
                                    }
                                }],
                                series: [
                                    {
                                        name: '技术评价',
                                        type: 'radar'
                                    }, {
                                        name: '市场分部',
                                        type: 'pie'
                                    },
                                    {
                                        name: '半径：转化交易率',
                                        type: 'scatter',
                                        symbolSize: function (data) {
                                            return (data[3] * 1000 + 8);
                                        },
                                        label: {
                                            emphasis: {
                                                show: true,
                                                formatter: function (param) {
                                                    return param.data[2];
                                                }
                                            }
                                        },
                                        itemStyle: {
                                            normal: {
                                                shadowBlur: 0,
                                                shadowOffsetY: 0,
                                                shadowOffsetX: 0,
                                                opacity: .8
                                            }
                                        }
                                    }
                                ]
                            },
                            options: CONSTDATA.yearsCombiOptions
                        });
                    }
                );

                // for (var i in CONSTDATA.yearsCombiOptions) {
                // for (var j in CONSTDATA.yearsCombiOptions[i]['series'][1]['data'][0]) {
                //     var value = angular.copy(CONSTDATA.yearsCombiOptions[i]['series'][2]['data'][j]);
                //     CONSTDATA.yearsCombiOptions[i]['series'][2]['data'][j] = {
                //       name: value.value[2],
                //       value: value.value,
                //       itemStyle: {normal: {color: areaColorSet[j]}}
                //     };
                //   }
                //     CONSTDATA.yearsCombiOptions[i]['series'][1]['data'][0][value]
                // }
                // console.log(JSON.stringify(CONSTDATA.yearsCombiOptions));

                return timeCombi;
            };
        }
    };
}]);
