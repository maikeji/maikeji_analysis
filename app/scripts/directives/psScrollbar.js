'use strict';
angular.module('MBD').directive('psScrollbar', [function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            Ps.initialize(element[0]);
        }
    };
}]);
