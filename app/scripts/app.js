'use strict';

/**
 * @ngdoc overview
 * @name MBD
 * @description
 * # MBD
 *
 * Main module of the application.
 */
angular
    .module('MBD', ['ngAnimate', 'ngResource', 'ngRoute', 'fullPage.js']).controller('IndexCtrl', ['$scope', '$timeout', '$q', 'API', 'CONSTDATA', 'mWechat',
    function ($scope, $timeout, $q, API, CONSTDATA, mWechat) {

        mWechat.share({
            title: '迈指数——迈科技旗下技术发展的数据智库',
            desc: '行业规律、未来趋势——让数据"说" 支持数据可视化定制服务'
        });

        var techHotMap, inOutBar, idMap, yearsChart, compareBar, marketData, yearsFun, devGartner,
            compareSequence = ["develop_rate", "average", "import_num", "export_sum", "sum", "import_sum", "transit_rate", "flexibility", "export_num", 'GDP', 'GDP贡献率'];

        var marketMapOption = {
                color: MetisPlugin.colorSet,
                tooltip: {
                    trigger: 'item',
                    formatter: function (params) {
                        return params.data.name + params.seriesName;
                    }
                },
                legend: {
                    orient: 'vertical',
                    y: 'bottom',
                    x: 'right',
                    data: ['技术输出总量', '技术吸纳总量'],
                    textStyle: {
                        color: '#fff'
                    },
                    selectedMode: 'single',
                    selected: {
                        '技术输出总量': true,
                        '技术吸纳总量': false
                    }
                },
                geo: {
                    map: 'china',
                    label: {
                        emphasis: {
                            show: false
                        }
                    },
                    selectedMode: 'single',
                    zoom: 1,
                    roam: 'scale',
                    scaleLimit: {
                        min: 1,
                        max: 2.5
                    },
                    itemStyle: {
                        normal: {
                            areaColor: '#333333',
                            borderColor: '#212121'
                        },
                        emphasis: {
                            areaColor: 'rgba(31,134,237,.6)'
                        }
                    },
                    regions: [{
                        name: '',
                        selected: true
                    }]
                },
                series: [
                    {
                        name: '技术输出总量',
                        type: 'scatter',//散点图
                        coordinateSystem: 'geo',//坐标系
                        data: [],
                        symbolSize: 6,
                        label: {
                            normal: {
                                show: false
                            }
                        },
                        itemStyle: {
                            normal: {
                                color: 'rgba(31,134,237,1)'
                            }
                        }
                    },
                    {
                        name: '技术吸纳总量',
                        type: 'scatter',//散点图
                        coordinateSystem: 'geo',//坐标系
                        data: [],
                        symbolSize: 6,
                        label: {
                            normal: {
                                show: false
                            }
                        },
                        itemStyle: {
                            normal: {
                                color: 'rgba(191,6,27,1)'
                            }
                        }
                    }
                ]
            },
            compareBarOption = {
                color: ['#0a539a', '#1f86ed'],
                grid: {
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0
                },
                xAxis: [{
                    type: 'value',
                    interval: 10,
                    max: 100,
                    axisLine: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    }
                }],
                yAxis: [{
                    type: 'category',
                    data: ['市场发展指数', '技术流动性', '技术吸纳量(件)', '技术输出额(亿元)', '技术交易总额(亿元)', '技术吸纳额(亿元)', '交易转化效率(%)', '平均技术交易额(亿元)', '技术输出量(件)', 'GDP(亿元)', 'GDP贡献率(%)'],
                    offset: -document.getElementById("marketCompareBar").clientWidth * .5 - 56,
                    axisLabel: {
                        textStyle: {
                            color: "#ffffff",
                            fontSize: 14
                        },
                        formatter: function (v, i) {
                            v = v.length === 5 ? v + ' ' + ' ' : (v.length === 6 ? v + ' ' : (v.length === 3 ? v + '      ' : v));
                            switch (v.length) {
                                case 5:
                                    v = v + '      ';
                                    break;
                                case 6:
                                    v = v + '     ';
                                    break;
                                case 7:
                                    v = v + '    ';
                                    break;
                                case 8:
                                    v = v + '   ';
                                    break;
                                case 9:
                                    v = v + '  ';
                                    break;
                                case 10:
                                    v = v + ' ';
                                    break;
                            }
                            return v;
                        }
                    },
                    axisLine: {
                        show: false
                    },
                    z: 3
                }],
                series: []
            },
            inOutBarOption = {
                color: ['#ff9d10', '#1f86ed', '#f86f00'],
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: ['技术输出', '技术吸纳', '技术出入比'],
                    bottom: 0,
                    textStyle: {
                        color: '#ffffff'
                    }
                },
                grid: {
                    top: 40,
                    bottom: 60,
                    right: 60,
                    left: 40
                },
                xAxis: [
                    {
                        name: '年份',
                        type: 'category',
                        data: ['2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016'],
                        axisLine: {
                            lineStyle: {
                                color: "#888888"
                            }
                        },
                        axisLabel: {
                            // show: false
                        },
                        axisTick: {
                            show: false
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '成交量(亿元)',
                        axisLabel: {
                            formatter: '{value} ml',
                            show: false
                        },
                        axisLine: {
                            lineStyle: {
                                color: "#888888"
                            }
                        },
                        splitLine: {
                            show: false
                        },
                        axisTick: {
                            show: false
                        }
                    },
                    {
                        type: 'value',
                        name: '',
                        axisLine: {
                            show: false
                        },
                        splitLine: {
                            show: false
                        },
                        axisLabel: {
                            show: false
                        },
                        axisTick: {
                            show: false
                        }
                    }
                ],
                series: [
                    {
                        name: '技术输出',
                        type: 'bar',
                        data: []
                    },
                    {
                        name: '技术吸纳',
                        type: 'bar',
                        data: []
                    },
                    {
                        name: '技术出入比',
                        type: 'line',
                        yAxisIndex: 1,
                        showAllSymbol: true,
                        data: []
                    }
                ]
            },
            yearsFunOption = {
                color: ['#8ed7e2', '#1f86ed'],
                smooth: true,
                grid: {
                    left: 0,
                    top: 30,
                    bottom: 30,
                    right: 60,
                    containLabel: true
                },
                title: {
                    text: '',
                    textStyle: {
                        color: '#fff',
                        fontSize: 14,
                        fontWeight: 'normal'
                    },
                    right: 10
                },
                tooltip: {
                    extraCssText: 'text-align:left',
                    formatter: function (params, data) {
                        return params.data[0] + '年' + params.seriesName + '<br>交易' + params.data[1] + '亿元' + '<br>交易量为' + params.data[2];
                    }
                },
                xAxis: {
                    name: '年份',
                    type: 'category',
                    data: [],
                    min: 'dataMin',
                    boundaryGap: false,
                    axisLabel: {
                        interval: 0
                    },
                    splitLine: {
                        show: false,
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#888'
                        }
                    }
                },
                yAxis: {
                    name: '亿元',
                    axisLine: {
                        lineStyle: {
                            color: '#888'
                        }
                    },
                    splitLine: {
                        show: false,
                    }
                },
                series: [{
                    name: '发展曲线',
                    type: 'line',
                    lineStyle: {
                        normal: {
                            width: 1
                        }
                    },
                    symbolSize: function (params) {
                        return Math.log(params[2], 5);
                    },
                    showAllSymbol: true,
                    itemStyle: {
                        emphasis: {
                            color: '#ff9d10'
                        }
                    },
                    data: [],
                    smooth: true
                }]
            };

        /**
         * 加特曲线函数
         * @param x
         * @returns {number}
         * @constructor
         */
        function Gartner(x) {
            if (x <= 1 && x >= .5) {
                return 3 * Math.pow(Math.E, -3 * (1.5 / x - 1.5) * (1 / x - 1));
            } else if (x >= 1 && x <= Math.pow(3, .5)) {
                return 2 - Math.sin(3 / 2 * Math.PI / x / x);
            } else if (x >= Math.pow(3, .5) && x <= 3) {
                return 3 - 2 * Math.sin(3 / 2 * Math.PI / x / x);
            }
        }

        function ObjectShrink(object, keynames) {
            var obj = {};
            for (var h = 0; h < keynames.length; h++) {
                obj[keynames[h]] = {};
                for (var i in object) {
                    obj[keynames[h]][i] = object[i][keynames[h]];
                }
            }
            return obj;
        };

        /**
         * 将对象转换成数组
         * @param object 目标对象
         * @param seq 顺序
         * @returns {Array}
         */
        function ObjectToArr(object, seq) {
            var arr = [];
            if (seq && seq.length > 0) {
                for (var i = 0; i < seq.length; i++) {
                    arr[i] = object[seq[i]];
                }
            }
            return arr;
        }

        /**
         * 将两个数组的对应位相除
         * @param arr1
         * @param arr2
         * @returns {Array}
         * @constructor
         */
        function ArrsToPercent(arr1, arr2) {
            if (arr1.length === arr2.length) {
                var arr = [];
                for (var i = 0; i < arr1.length; i++) {
                    arr[i] = arr2[i] === 0 ? 0 : Math.round(arr1[i] / arr2[i] * 100) / 100;
                }
                return arr;
            }
        };

        var vm = this;
        vm.heatProvince = '上海';
        vm.market = {
            dpd: {
                left: {
                    name: '上海',
                    show: false
                },
                right: {
                    name: '北京',
                    show: false
                },
                data: ['上海', '河北', '山西', '内蒙古', '辽宁', '吉林', '黑龙江', '江苏', '浙江', '安徽', '福建', '江西', '山东', '河南', '湖北', '湖南', '广东', '广西', '海南', '四川', '贵州', '云南', '西藏', '陕西', '甘肃', '青海', '宁夏', '新疆', '北京', '天津', '重庆'],
                Select: function (pos, name, e) {
                    if (vm.market.dpd[pos] && vm.market.dpd[pos].name !== name) {
                        vm.market.dpd[pos].name = name;
                        vm.market.dpd[pos].show = false;

                        DrawComparallel(2);

                        e.stopPropagation();
                    }
                }
            },
            scatter: {
                selected: 0
            }
        };
        vm.companyInfo = {};

        //省份选择下拉框
        vm.province = {
            index: 0,
            name: 'shanghai',
            cname: '上海'
        };

        vm.devArea = {
            items: [['材料科技', '机械电子', '化学化工'], ['能源电力', '环境工程', '生农医药']],
            selected: '能源电力',
            Select: function () {
            }
        };

        // vm.yearsFunName = '';

        vm.fullpageOptions = {
            //Navigation
            menu: '#menu',
            lockAnchors: false,
            // anchors: ['firstPage', 'secondPage'],
            navigation: true,
            navigationPosition: 'left',
            // navigationTooltips: ['firstSlide', 'secondSlide'],
            showActiveTooltip: false,
            slidesNavigation: false,
            slidesNavPosition: 'bottom',
            sectionsColor: ['#333', '#4A4A4A', '#333', '#4A4A4A', '#333', '#4A4A4A'],

            //Scrolling
            css3: true,
            scrollingSpeed: 500,
            autoScrolling: true,
            fitToSection: true,
            fitToSectionDelay: 1000,
            scrollBar: true,
            easing: 'easeInOutCubic',
            easingcss3: 'ease',
            loopBottom: false,
            loopTop: false,
            loopHorizontal: true,
            continuousVertical: false,
            continuousHorizontal: false,
            scrollHorizontally: false,
            interlockedSlides: false,
            dragAndMove: false,
            offsetSections: false,
            resetSliders: false,
            fadingEffect: false,
            normalScrollElements: '#element1, .element2',
            scrollOverflow: false,
            scrollOverflowReset: false,
            scrollOverflowOptions: null,
            touchSensitivity: 15,
            normalScrollElementTouchThreshold: 5,
            bigSectionsDestination: null,

            //Accessibility
            keyboardScrolling: true,
            animateAnchor: true,
            recordHistory: true,

            //Design
            controlArrows: true,
            verticalCentered: true,
            //Custom selectors
            sectionSelector: '.section',
            slideSelector: '.slide',

            lazyLoading: true,

            //events
            onLeave: function (index, nextIndex, direction) {
            },
            afterLoad: function (anchorLink, index) {
            },
            afterRender: function () {
            },
            afterResize: function () {
            },
            afterResponsive: function (isResponsive) {
            },
            afterSlideLoad: function (anchorLink, index, slideAnchor, slideIndex) {
            },
            onSlideLeave: function (anchorLink, index, slideIndex, direction, nextSlideIndex) {
                // console.log('leave');
            }
        };

        /**
         * 获取行业推荐数据
         * @param year
         * @returns {*}
         */
        function GetIndustryRec(from, to) {
            return API.industryRecommended({
                year: from + '-' + to
            }).$promise;
        };

        /**
         * 请求当地特色产业数据
         * 1。[产业]
         * 2。[产业关键字]
         */
        function GetLocalKeyIndustry(province, city) {
            return API.localKeyIndustry({
                'province': province, 'city': city
            }).$promise;
        };

        /**
         * 请求地区技术参数
         * @param province string 省
         * @returns {*}
         */
        function GetProvinceParam(province) {
            return API.techTradeRegion({
                province: province
            }).$promise;
        }

        /**
         * 请求区域热度
         * @param loc 省份,为空则返回全国
         * @returns {*}
         */
        function GetHeatDistribution(loc) {
            return API.marketHeat({
                province: loc
            }).$promise;
        }

        /**
         * 请求技术的逐年发展参数
         * @param industry 技术名
         * @returns {*}
         */
        function GetIndustryDevPoint(industry) {
            return API.industryDevelopment({
                industry: industry
            }).$promise;
        };

        /**
         * 绘制柱状对比图
         * @param times 初始化次数
         */
        function DrawComparallel(times) {
            $q.all([GetProvinceParam(vm.market.dpd.left.name), GetProvinceParam(vm.market.dpd.right.name)]).then(function (data) {
                var leftArr = ObjectToArr(data[0].toJSON(), compareSequence), rightArr = ObjectToArr(data[1].toJSON(), compareSequence);
                if (leftArr.length === rightArr.length) {
                    for (var i = 0; i < leftArr.length; i++) {
                        var total = leftArr[i] + rightArr[i];
                        leftArr[i] = leftArr[i] / total * 100;
                        rightArr[i] = rightArr[i] / total * 100;
                    }
                }
                // compareBarOption.c = data;
                compareBarOption.tooltip = {
                    trigger: 'axis',
                    axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                        type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                    },
                    extraCssText: 'text-align:left',
                    formatter: function (params, ticket, callback) {
                        var left = data[0];
                        var right = data[1];
                        return params[0].name + '<br/>' +
                            params[0].seriesName.substr(1) + '：' +
                            (Number.isInteger(left[compareSequence[params[0].dataIndex]]) ? left[compareSequence[params[0].dataIndex]] : parseFloat(left[compareSequence[params[0].dataIndex]]).toFixed(2)) + '<br/>' +
                            params[1].seriesName.substr(1) + '：' +
                            (Number.isInteger(right[compareSequence[params[1].dataIndex]]) ? right[compareSequence[params[1].dataIndex]] : parseFloat(right[compareSequence[params[1].dataIndex]]).toFixed(2));
                    }
                };
                compareBarOption.series = [{
                    name: '左' + vm.market.dpd.left.name,
                    type: 'bar',
                    barWidth: 20,
                    stack: '总量',
                    label: {
                        normal: {
                            show: false
                        }
                    },
                    data: leftArr
                }, {
                    name: '右' + vm.market.dpd.right.name,
                    type: 'bar',
                    barWidth: 20,
                    stack: '总量',
                    label: {
                        normal: {
                            show: false
                        }
                    },
                    data: rightArr
                }];

                //对比柱状图
                if (times > 1) {
                    compareBar.setOption(compareBarOption, true);
                } else {
                    $(document.getElementById("marketCompareBar")).height(360);
                    compareBar = MetisPlugin.DrawCompareBar(document.getElementById("marketCompareBar"), null, compareBarOption);
                }
            });
        }

        /**
         * 获取并显示公司详情
         * @param name 公司名称
         * @returns {*}
         */
        function ShowCompanyInfo(name) {
            vm.companyInfo.name = name;

            return API.companyInfo({
                name: name
            }).$promise;
        }

        /**
         * 绘制相关机构关系图和公司信息
         * @param times 数字
         */
        function DrawRelatedIns(times) {
            // console.log(vm.province.cname, vm.city.cname, vm.city.keywords.selected);
            //todo:出错处理
            API.relatedIns({
                'province': vm.province.cname,
                'city': vm.city.cname,
                'industry': vm.city.keywords.selected
            }, function (data) {
                data = data.toJSON();

                if (data['公司'].length > 0) {
                    ShowCompanyInfo(data['公司'][Object.keys(data['公司'])[0]]).then(function (data) {
                        if (data['名称']) {
                            vm.companyInfo = data.toJSON();
                            vm.companyInfo.show = true;
                            vm.companyInfo['经营范围'] = vm.companyInfo['经营范围'].length > 80 ? vm.companyInfo['经营范围'].substring(0, 80) + '...' : vm.companyInfo['经营范围'];
                        } else {
                            vm.companyInfo = data.toJSON();
                        }
                    });
                } else {
                    console.log();
                }

                var category = ['公司'], n1 = data['公司'].slice(0, 10);
                // for (var i in data) {
                //     category.push(i);
                // n1 = n1.concat(Object.keys(data[i]));
                // }

                //省份特色产业相关机构
                MetisPlugin.DrawDependencyGraph(document.getElementById("companyGraph"), {
                    data: {
                        n0: vm.city.keywords.selected,
                        n1: n1
                    }, category: category
                });

                //todo:听说要超链接！！！
                if (times > 1) {
                    document.getElementById("companyGraph").removeChild(document.getElementById("companyGraph").childNodes[0]);
                    document.getElementById("companyGraph").removeChild(document.getElementById("companyGraph").childNodes[0]);
                }
            }, function (err) {
                console.log('err:', err);
            });
        }

        function DrawKeywordsDevLines(data) {

            var series = [];

            for (var i in data) {
                var pos = [];

                for (var j in data[i]) {
                    pos.push([j, data[i][j]]);
                }

                series.push({
                    name: i,
                    type: 'line',
                    lineStyle: {
                        normal: {
                            width: 2
                        }
                    }, symbolSize: 8,
                    // symbolSize: function (params) {
                    //     return Math.pow(params[2], .5);
                    // },
                    data: pos,
                    smooth: true
                });
            }

            $('#multiFunction').height($('#multiFunction').width() / 2.3);
            MetisPlugin.DrawFunction(document.getElementById("multiFunction"), null, null,
                {
                    color: ['#2b8ff2', '#00ffbe', '#ffc760', '#4fccff', '#ff5722', '#fb497c'],
                    smooth: true,
                    backgroundColor: 'rgba(0,0,0,0)',
                    grid: {
                        top: 30,
                        left: '6%',
                        right: '6%'
                    },
                    legend: {
                        bottom: 0,
                        data: Object.keys(data),
                        textStyle: {
                            color: '#f1f1f1'
                        }
                    },
                    xAxis: {
                        type: 'value',
                        name: '年份',
                        nameTextStyle: {
                            color: '#ddd'
                        },
                        splitLine: {
                            show: false,
                        },
                        axisTick: {
                            show: false
                        },
                        axisLine: {
                            show: false
                        },
                        max: 'dataMax',
                        min: 'dataMin',
                        splitNumber: 13,
                        axisLabel: {
                            // show: false
                            textStyle: {
                                color: '#ddd'
                            },
                            formatter: function (params) {
                                // console.log(params);
                                return params + ' ';
                            }
                        }
                    },
                    yAxis: {
                        type: 'value',
                        name: '相关技术成果数量(件)',
                        nameTextStyle: {
                            color: '#ddd'
                        },
                        axisLine: {
                            // show: false
                            lineStyle: {
                                color: '#ddd'
                            }
                        },
                        splitLine: {
                            show: false,
                            lineStyle: {
                                opacity: .3
                            }
                        },
                        min: 0,
                        axisTick: {
                            show: false
                        },
                        axisLabel: {
                            show: false
                        }
                    },
                    series: series
                });
        }


        /**
         * 绘制行业发展规律图
         */
        function DrawIndustryRec() {
            GetIndustryRec('2005', '2015').then(function (data) {
                var series = [{
                    type: 'scatter',
                    name: '10年后',
                    itemStyle: {normal: {opacity: 1}},
                    data: []
                }, {
                    type: 'scatter',
                    name: '5-10年',
                    itemStyle: {normal: {opacity: 1}},
                    data: []
                }, {
                    type: 'scatter',
                    name: '2-5年',
                    itemStyle: {normal: {opacity: 1}},
                    data: []
                }];

                //整理数据
                for (var i in CONSTDATA.techGartner) {
                    if (CONSTDATA.techGartner[i].apply_time === '10年后') {
                        series[0].data.push([CONSTDATA.techGartner[i].position, Gartner(CONSTDATA.techGartner[i].position), i, CONSTDATA.techGartner[i].keywords]);
                    } else if (CONSTDATA.techGartner[i].apply_time === '5-10年') {
                        series[1].data.push([CONSTDATA.techGartner[i].position, Gartner(CONSTDATA.techGartner[i].position), i, CONSTDATA.techGartner[i].keywords]);
                    } else if (CONSTDATA.techGartner[i].apply_time === '2-5年') {
                        series[2].data.push([CONSTDATA.techGartner[i].position, Gartner(CONSTDATA.techGartner[i].position), i, CONSTDATA.techGartner[i].keywords]);
                    }
                }

                $('#tradeGartnerFunction').height($('#tradeGartnerFunction').width() / 1.6);
                $('.trade-gartner-bg').width($('#tradeGartnerFunction').width());
                $('.trade-gartner-bg').css('left', $('#tradeGartnerFunction').width() * 1 / 8 + 15);
                $('.trade-gartner-bg').height($('#tradeGartnerFunction').width() / 1.6);
                devGartner = MetisPlugin.DrawFunction(document.getElementById("tradeGartnerFunction"), null, [Gartner],
                    {
                        color: ['#f8b551', '#8ed7e2', '#aaaaaa'],
                        smooth: true,
                        legend: {
                            right: 5,
                            selectedMode: false,
                            data: [{
                                name: '10年后', textStyle: {
                                    color: '#ddd'
                                }
                            }, {
                                name: '5-10年', textStyle: {
                                    color: '#ddd'
                                }
                            }, {
                                name: '2-5年', textStyle: {
                                    color: '#ddd'
                                }
                            }]
                        },
                        grid: {
                            top: '8%',
                            left: 0,
                            right: '1%',
                            bottom: '22.2%'
                        },
                        xAxis: {
                            splitLine: {
                                show: false,
                                lineStyle: {
                                    type: 'dashed',
                                    color: '#ddd'
                                }
                            },
                            axisTick: {
                                show: false
                            },
                            axisLabel: {
                                show: false
                            },
                            axisLine: {
                                show: false
                            },
                            max: 'dataMax',
                            min: 'dataMin'
                        },
                        yAxis: {
                            axisLine: {
                                show: false
                            },
                            splitLine: {
                                show: false
                            },
                            axisTick: {
                                show: false
                            },
                            axisLabel: {
                                show: false
                            }
                        },
                        series: series
                    });

                devGartner.on('click', function (params) {
                    window.open('http://metis.maikeji.cn/technologies/' + (params.data[3] && params.data[3].length > 0 ? params.data[3].join(' ') : params.data[2]));
                });

                vm.devArea.Select = function (item) {
                    vm.devArea.selected = item;
                    DrawKeywordsDevLines(data[item]);
                }

                DrawKeywordsDevLines(data['能源电力']);
            });
        };

        (function () {

            $timeout(function () {
                yearsChart = $scope.InitTimeCombi();

                GetIndustryDevPoint(CONSTDATA.yearsAreas.name[0]).then(function (data) {
                    $('#gartnerFunction').height(360);

                    yearsFunOption.title.text = CONSTDATA.yearsAreas.name[0];
                    yearsFunOption.series[0].name = CONSTDATA.yearsAreas.name[0];
                    yearsFunOption.series[0].lineStyle.normal.color = CONSTDATA.yearsAreas.color[0];
                    yearsFunOption.xAxis.data = Object.keys(data.toJSON());

                    yearsFun = MetisPlugin.DrawFunction(document.getElementById("gartnerFunction"), data.toJSON(), [function (x) {
                        if (x <= 1 && x >= .5) {
                            return 3 * Math.pow(Math.E, -3 * (1 / x - 1) * (1 / x - 1));
                        } else if (x >= 1 && x <= Math.pow(3, .5)) {
                            return 2 - Math.sin(3 / 2 * Math.PI / x / x);
                        } else if (x >= Math.pow(3, .5) && x <= 3) {
                            return 3 - 2 * Math.sin(3 / 2 * Math.PI / x / x);
                        }
                    }], yearsFunOption);
                });

                yearsChart.on('click', function (params) {
                    if (params.seriesType === 'scatter') {
                        //显示当前领域的gartner曲线
                        yearsChart.dispatchAction({
                            type: 'timelinePlayChange',
                            playState: false
                        });

                        GetIndustryDevPoint(params.name).then(function (result) {
                            var data = [];

                            for (var i in result.toJSON()) {
                                data.push([i, result[i].trade_sum, result[i].trade_num]);
                            }


                            yearsFunOption.title.text = params.name;
                            yearsFunOption.series = [{
                                name: params.name,
                                type: 'line',
                                lineStyle: {
                                    normal: {
                                        width: 1,
                                        color: CONSTDATA.yearsAreas.color[CONSTDATA.yearsAreas.name.indexOf(params.name) !== -1 ? CONSTDATA.yearsAreas.name.indexOf(params.name) : 0]
                                    }
                                },
                                symbolSize: function (params) {
                                    return Math.log(params[2], 5);
                                },
                                data: data,
                                smooth: true
                            }];

                            yearsFun.setOption(yearsFunOption, true);
                        });
                    }
                });

                yearsChart.on('mouseover', function (params) {
                    if (params.seriesType === 'scatter') {
                        yearsChart.dispatchAction({
                            type: 'timelinePlayChange',
                            playState: false
                        });
                    }
                });

                yearsChart.on('mouseout', function (params) {
                    if (params.seriesType === 'scatter') {
                        yearsChart.dispatchAction({
                            type: 'timelinePlayChange',
                            playState: true
                        });
                    }
                });
            });

            GetHeatDistribution().then(function (data) {
                marketData = ObjectShrink(data.toJSON(), ['export_rate', 'export_sum', 'import_rate', 'import_sum', 'tech_export', 'tech_import']);
                marketMapOption.visualMap = {
                    min: 800,
                    max: 50000,
                    text: ['High', 'Low'],
                    realtime: false,
                    calculable: true,
                    inRange: {
                        color: ['lightskyblue', 'yellow', 'orangered']
                    }
                };
                techHotMap = MetisPlugin.DrawHeatMap(document.getElementById("techScatterMap"), [marketData.export_sum, marketData.import_sum], CONSTDATA.MapShape.china, '总量');
                techHotMap.on('click', function (params) {
                    if (params.name) {
                        vm.heatProvince = params.name;
                        $scope.$apply();

                        inOutBarOption.series[0].data = marketData.tech_export[params.name];
                        inOutBarOption.series[1].data = marketData.tech_import[params.name];
                        inOutBarOption.series[2].data = ArrsToPercent(marketData.tech_export[params.name], marketData.tech_import[params.name]);
                        inOutBar.setOption(inOutBarOption, true);
                    }
                });
                vm.market.scatter.Select = function (number) {
                    // console.log(number);
                    if (number !== vm.market.scatter.selected) {
                        vm.market.scatter.selected = number;
                        if (number === 0) {
                            techHotMap = MetisPlugin.DrawHeatMap(document.getElementById("techScatterMap"), [marketData.export_sum, marketData.import_sum], CONSTDATA.MapShape.china, '总量');
                            techHotMap.on('click', function (params) {
                                if (params.name) {
                                    vm.heatProvince = params.name;
                                    $scope.$apply();

                                    inOutBarOption.series[0].data = marketData.tech_export[params.name];
                                    inOutBarOption.series[1].data = marketData.tech_import[params.name];
                                    inOutBarOption.series[2].data = ArrsToPercent(marketData.tech_export[params.name], marketData.tech_import[params.name]);
                                    inOutBar.setOption(inOutBarOption, true);
                                }
                            });
                        } else if (number === 1) {
                            techHotMap = MetisPlugin.DrawHeatMap(document.getElementById("techScatterMap"), [marketData.export_rate, marketData.import_rate], CONSTDATA.MapShape.china, '增长率');
                            techHotMap.on('click', function (params) {
                                if (params.name) {
                                    vm.heatProvince = params.name;
                                    $scope.$apply();
                                    inOutBarOption.series[0].data = marketData.tech_export[params.name];
                                    inOutBarOption.series[1].data = marketData.tech_import[params.name];
                                    inOutBarOption.series[2].data = ArrsToPercent(marketData.tech_export[params.name], marketData.tech_import[params.name]);
                                    inOutBar.setOption(inOutBarOption, true);
                                }
                            });
                        }
                    }
                };

                inOutBarOption.series[0].data = marketData.tech_export[vm.province.cname];
                inOutBarOption.series[1].data = marketData.tech_import[vm.province.cname];
                inOutBarOption.series[2].data = ArrsToPercent(marketData.tech_export[vm.province.cname], marketData.tech_import[vm.province.cname]);

                inOutBar = MetisPlugin.DrawLineBar(document.getElementById("techInOutBar"), null, inOutBarOption);
            });

            DrawComparallel(1);

            DrawIndustryRec();

            angular.element("#companyGraph").on('click', function (e) {
                // console.log(e.target);
                if (e.target.nodeName === 'rect') {
                    ShowCompanyInfo(angular.element(e.target).next().children()[0].innerHTML).then(function (data) {
                        if (data['名称']) {
                            vm.companyInfo = data.toJSON();
                            vm.companyInfo.show = true;
                            vm.companyInfo['经营范围'] = vm.companyInfo['经营范围'].length > 80 ? vm.companyInfo['经营范围'].substring(0, 80) + '...' : vm.companyInfo['经营范围'];
                        } else {
                            vm.companyInfo = data.toJSON();
                        }
                    });
                } else if (e.target.nodeName === 'text') {
                    ShowCompanyInfo(angular.element(e.target).children()[0].innerHTML).then(function (data) {
                        if (data['名称']) {
                            vm.companyInfo = data.toJSON();
                            vm.companyInfo.show = true;
                            vm.companyInfo['经营范围'] = vm.companyInfo['经营范围'].length > 80 ? vm.companyInfo['经营范围'].substring(0, 80) + '...' : vm.companyInfo['经营范围'];
                        } else {
                            vm.companyInfo = data.toJSON();
                        }
                    });
                }
            });
        })();

    }]);
